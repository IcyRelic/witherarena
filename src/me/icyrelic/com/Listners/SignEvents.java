package me.icyrelic.com.Listners;

import me.icyrelic.com.WitherArena;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class SignEvents implements Listener{
	

	WitherArena plugin;
	public SignEvents(WitherArena instance) {

		plugin = instance;

		}
	
	@EventHandler
	public void onSignChange(SignChangeEvent e){
		
		 if (e.getBlock().getType() == Material.WALL_SIGN || e.getBlock().getType() == Material.SIGN || e.getBlock().getType() == Material.SIGN_POST ) {
			   if (e.getLine(0).equalsIgnoreCase("[ready]")) {
			    e.setLine(0, ChatColor.WHITE + "[Ready]");
			    e.setLine(1, "");
			    e.setLine(2, "Right Click To");
			    e.setLine(3, "Ready Up!");
			   }
		 }
		
	}
	
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent e){
		 Player p = e.getPlayer();
		 
		 if (!e.hasBlock()){
		            return;
			 }
		 
		 final Action action = e.getAction();
		 if (action == Action.RIGHT_CLICK_BLOCK) {
			 
			 if(e.getClickedBlock().getState() instanceof Sign){
				 Sign sign = (Sign) e.getClickedBlock().getState();
				 if (sign.getLine(0).equals(ChatColor.WHITE + "[Ready]")) {
					 
					 if(plugin.getPlayerManager().getPlayer(p.getName()).isInArena()){
							if(plugin.getPlayerManager().getPlayer(p.getName()).isInArena()){
								plugin.getArenaManager().readyPlayer(p, plugin.getPlayerManager().getPlayer(p.getName()).getPlayerArena());
							}
					 }
					 
				 }else  if(plugin.getPlayerManager().getPlayer(p.getName()).isInLobby()){
					 
					 if(plugin.getClassManager().getClass(sign.getLine(0)).exists()){
						 if(plugin.getClassManager().getClass(sign.getLine(0)).needsPermission()){
							 if(p.hasPermission("")){
								 plugin.getClassManager().selectClass(p, sign.getLine(0), sign);	
							 }else{
								 p.sendMessage(plugin.prefix + ChatColor.RED + "You dont have permission to use this class");
							 }
						 }else{
							 plugin.getClassManager().selectClass(p, sign.getLine(0), sign);	
						 }
					 }
					 
					 
				 }
			 }
			 
		 }

	}

}
