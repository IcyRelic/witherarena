package me.icyrelic.com.Listners;

import me.icyrelic.com.WitherArena;

import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.Plugin;

import com.sk89q.worldguard.bukkit.WorldGuardPlugin;

public class ArenaListeners implements Listener {

	WitherArena plugin;
	public ArenaListeners(WitherArena instance) {

		plugin = instance;

		}
	
	@EventHandler
	public void onPlayerCommand(PlayerCommandPreprocessEvent e){
		
		String base = e.getMessage().split(" ")[0];
        String noslash = base.substring(1);
        
        if(plugin.getPlayerManager().getPlayer(e.getPlayer().getName()).isInArena() && !plugin.getMaster().isAllowed(noslash)){
        	e.getPlayer().sendMessage(plugin.prefix + "You are not alloud to use that command in the arena!");
        	e.setCancelled(true);
        }
		
	}
	@EventHandler
	public void onEntityExplode(EntityExplodeEvent e){
		Entity entity = e.getEntity();
		int x = entity.getLocation().getBlockX();
   		int y = entity.getLocation().getBlockY();
   		int z = entity.getLocation().getBlockZ();
   		World world = entity.getWorld();
   		
   		int arena = 0;
   		while(arena < plugin.getArenaManager().getArenas().size()){
   			
   			String arenaName = plugin.getArenaManager().getArenas().get(arena);
   			if(plugin.getArenaManager().getArena(arenaName).exists()){
   				
   				if(plugin.getArenaManager().getArena(arenaName).isRunning()){
   					String region = plugin.getConfig().getString("arenas."+arenaName+".region");
   		   			boolean isRegion = getWorldGuard().getRegionManager(world).hasRegion(region);
   		   	   		if(isRegion){
   		   	   	   		if(getWorldGuard().getRegionManager(world).getRegion(region).contains(x, y, z)){

   		   	   	   			
   		   	   	   			e.setCancelled(true);   		   	   	   			
   		   	   	   			
   		   	   	   		}
   		   	   	   		
   		   	   		}	
   				}
   				
   			}

   			
   			arena++;
   		}

   	   			
		
	}
	
	@EventHandler
	public void onPlayerDamage(EntityDamageEvent e){
		
		if(e.getEntity() instanceof Player){
			Player p = (Player) e.getEntity();
			if(plugin.getPlayerManager().getPlayer(p.getName()).isInArena()){
				if(plugin.getPlayerManager().getPlayer(p.getName()).isInArena()){
					e.setCancelled(true);
				}
			}
		}
		
	}
	
	@EventHandler
    public void onEntityDeath(EntityDeathEvent e)
    {
		if(plugin.getArenaManager().getArena("").entityHasArena(e.getEntity().getEntityId())){
			String arena = plugin.getArenaManager().getArena("").getWithersArena(e.getEntity().getEntityId());
			World w = plugin.getServer().getWorlds().get(0);
			plugin.getArenaManager().endArena(arena, w);
			e.getDrops().clear();
		}
		
		if(e.getEntityType() == EntityType.ZOMBIE){
			
			int x = 0;
			
			while(x < plugin.getArenaManager().getArena("").getZombies().size()){
				String[] str = plugin.getArenaManager().getArena("").getZombies().get(x).split("_");
				
				String arena = str[0];
				String id = str[1];
				
				if(e.getEntity().getEntityId() == Integer.parseInt(id)){
					e.getDrops().clear();
					plugin.getArenaManager().getArena(arena).removeZombie(Integer.parseInt(id));
				}
				
				x++;
				
			}
			
		}
		

    }
	@EventHandler
    public void onPlayerDeath(PlayerDeathEvent e)
    {
		Player p = e.getEntity();
		if(plugin.getPlayerManager().getPlayer(p.getName()).isInArena()){
			p.setHealth(20);
			
			e.setDeathMessage("");
			
			p.sendMessage(plugin.prefix + "You have died!");
			plugin.getArenaManager().removePlayer(p, true, plugin.getPlayerManager().getPlayer(p.getName()).getPlayerArena());
			e.getDrops().clear();
			
			
		}
    }
	@EventHandler
	public void onPlayerDropItem(PlayerDropItemEvent e) {
        Player p = e.getPlayer();
        if(plugin.getPlayerManager().getPlayer(p.getName()).isInArena()){
        	e.setCancelled(true);
        }
        
 }
	@EventHandler
    public void onPlayerQuit(PlayerQuitEvent e)
    {
		Player p = e.getPlayer();
		if(plugin.getPlayerManager().getPlayer(p.getName()).isInArena()){
			plugin.getArenaManager().removePlayer(p, true, plugin.getPlayerManager().getPlayer(p.getName()).getPlayerArena());
		}
		
    }
	@EventHandler
    public void onPlayerJoin(PlayerJoinEvent e)
    {
		Player p = e.getPlayer();
		
		if(plugin.getPlayerManager().getPlayer(p.getName()).isInArena()){
			plugin.getArenaManager().removePlayer(p, true, plugin.getPlayerManager().getPlayer(p.getName()).getPlayerArena());
		}
    }
	@EventHandler
    public void onBlockBreak(BlockBreakEvent e)
    {
		Player p = e.getPlayer();
		if(plugin.getPlayerManager().getPlayer(p.getName()).isInArena()){
			
			e.setCancelled(true);
			
		}
    }
	@EventHandler
    public void onBlockPlace(BlockPlaceEvent e)
    {
		
		Player p = e.getPlayer();
		if(plugin.getPlayerManager().getPlayer(p.getName()).isInArena()){
			
			e.setCancelled(true);
			
		}
		
    }
	@EventHandler
	public void onFoodLevelChange(FoodLevelChangeEvent e){
		if(e.getEntity() instanceof Player){
			
			Player p = (Player) e.getEntity();
			if(plugin.getPlayerManager().getPlayer(p.getName()).isInArena()){
				e.setCancelled(true);
			}
			
		}
	}

	
	private WorldGuardPlugin getWorldGuard() {
	    Plugin pl = plugin.getServer().getPluginManager().getPlugin("WorldGuard");
	 
	    // WorldGuard may not be loaded
	    if (pl == null || !(pl instanceof WorldGuardPlugin)) {
	        return null; // Maybe you want throw an exception instead
	    }
	 
	    return (WorldGuardPlugin) pl;
	}
	
}
