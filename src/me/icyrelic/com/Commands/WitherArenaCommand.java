package me.icyrelic.com.Commands;





import java.util.ArrayList;

import me.icyrelic.com.WitherArena;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;


public class WitherArenaCommand implements CommandExecutor {
	
	
	String noPerm = (ChatColor.RED + "You do not have permission!");
	
	WitherArena plugin;
	public WitherArenaCommand(WitherArena instance) {

		plugin = instance;

	}
	
	public void showPlayerHelp(Player p){
		
		p.sendMessage(plugin.prefix + "/witherarena help - Shows this");
		p.sendMessage(plugin.prefix + "/witherarena join (arena) - Join an arena");
		p.sendMessage(plugin.prefix + "/witherarena leave - Leave your current arena");
		p.sendMessage(plugin.prefix + "/witherarena list [arenas | classes] - Show a list of arenas/classes");
		p.sendMessage(plugin.prefix + "/witherarena selectarena - Select an arena");
		p.sendMessage(plugin.prefix + "/witherarena create [arena | class] [name] - Create a new arena/class");
		p.sendMessage(plugin.prefix + "/witherarena delete [arena | class] [name] - Delete an arena/class");
		p.sendMessage(plugin.prefix + "/witherarena checkdata - Check data needed to create the currently selected arena");
		p.sendMessage(plugin.prefix + "/witherarena set [warp | spawn | region] - Set warps/spawns/region for the selected arena");
		p.sendMessage(plugin.prefix + "/witherarena players (arena) - Shows the players in an arena");
		p.sendMessage(plugin.prefix + "/witherarena reload - Reload the config");
		
	}
	
	
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		

		if (cmd.getName().equalsIgnoreCase("witherarena")) {
			if(sender instanceof Player){

				Player p = (Player) sender;
				
				if(args.length >= 1){

					
					if(args[0].equalsIgnoreCase("checkdata")){
						if(p.hasPermission("witherarena.admin.checkdata")){
							
							plugin.getArenaManager().checkData(p);
							
						}else{
							p.sendMessage(noPerm);
						}
						
					}
					
					
					else if(args[0].equalsIgnoreCase("help")){
						if(p.hasPermission("witherarena.user.help")){
							showPlayerHelp(p);
						}else{
							p.sendMessage(noPerm);
						}
						
					}
					
					else if(args[0].equalsIgnoreCase("players")){
						if(p.hasPermission("witherarena.user.players")){
							if(args.length == 2){
								//supplied arena
								String arena = args[1];
								if(plugin.getArenaManager().getArena(arena).exists()){
									ArrayList<String> players = plugin.getArenaManager().getArena(arena).getPlayers();
									
									String str = "";
									
									int x = 0;
									boolean first = true;
									while(x < players.size()){
										
										if(plugin.getPlayerManager().getPlayer(players.get(x)).isReady()){
											if(first){
												str = (ChatColor.GREEN + players.get(x));
												first = false;
											}else{
												str = str+ChatColor.WHITE+", "+ChatColor.GREEN+players.get(x);
											}
										}else{
											if(first){
												str = (ChatColor.RED + players.get(x));
												first = false;
											}else{
												str = (str+ChatColor.WHITE+", "+ChatColor.RED+players.get(x));
											}
										}

										x++;
									}
									
									p.sendMessage(plugin.prefix + ChatColor.GRAY + "Players("+arena+"): "+str);
								}else{
									p.sendMessage(plugin.prefix + ChatColor.RED +"Error: That arena does not exist");
								}
							}else{
								//own arena
								if(plugin.getPlayerManager().getPlayer(p.getName()).isInArena()){
									String arena = plugin.getPlayerManager().getPlayer(p.getName()).getPlayerArena();
									if(plugin.getArenaManager().getArena(arena).exists()){
										ArrayList<String> players = plugin.getArenaManager().getArena(arena).getPlayers();
										
										String str = "";
										
										int x = 0;
										boolean first = true;
										while(x < players.size()){
											
											if(plugin.getPlayerManager().getPlayer(players.get(x)).isReady()){
												if(first){
													str = (ChatColor.GREEN + players.get(x));
													first = false;
												}else{
													str = str+ChatColor.WHITE+", "+ChatColor.GREEN+players.get(x);
												}
											}else{
												if(first){
													str = (ChatColor.RED + players.get(x));
													first = false;
												}else{
													str = (str+ChatColor.WHITE+", "+ChatColor.RED+players.get(x));
												}
											}

											x++;
										}
										
										p.sendMessage(plugin.prefix + ChatColor.GRAY + "Players("+arena+"): "+str);
									}else{
										p.sendMessage(plugin.prefix + ChatColor.RED +"Error: That arena does not exist");
									}
								}else{
									p.sendMessage(plugin.prefix + ChatColor.RED +"Error: You are not in an arena");
								}
							}
							
						}else{
							p.sendMessage(noPerm);
						}
						
					}
					
					else if(args[0].equalsIgnoreCase("reload")){
						if(p.hasPermission("witherarena.admin.reload")){
							
							plugin.reloadConfig();
							p.sendMessage(plugin.prefix + "Config Reloaded");
							
						}else{
							p.sendMessage(noPerm);
						}
						
					}
					
					else if(args[0].equalsIgnoreCase("join") || args[0].equalsIgnoreCase("j")){
						
						if(p.hasPermission("witherarena.user.join")){
							
							if(args.length == 2){
								plugin.getArenaManager().addPlayer(p, args[1]);
							}else if (args.length == 1){
								plugin.getArenaManager().tryJoinDefault(p);
							}else{
								p.sendMessage(ChatColor.RED + "Usage: /witherarena join <arena>");
							}
							
						}else{
							p.sendMessage(noPerm);
						}
						
					}
					
					else if(args[0].equalsIgnoreCase("leave") || args[0].equalsIgnoreCase("l")){
						
						if(p.hasPermission("witherarena.user.leave")){
							
							plugin.getArenaManager().removePlayer(p, true, plugin.getPlayerManager().getPlayer(p.getName()).getPlayerArena());
							
						}else{
							p.sendMessage(noPerm);
						}
						
					}
					
					else if(args[0].equalsIgnoreCase("list")){
						if(args.length == 2){
							if(args[1].equalsIgnoreCase("classes")){
								if(p.hasPermission("witherarena.user.listclasses")){
									plugin.getClassManager().listClasses(p);
								}else{
									p.sendMessage(noPerm);
								}
								
							}else if(args[1].equalsIgnoreCase("arenas")){
								if(p.hasPermission("witherarena.user.listarenas")){
									plugin.getArenaManager().listArenas(p);
								}else{
									p.sendMessage(noPerm);
								}
							}
						}else{
							sender.sendMessage(ChatColor.RED + "Usage: /witherarena list <classes | arenas>");
						}

					}
					
					
					else if(args[0].equalsIgnoreCase("selectarena") || args[0].equalsIgnoreCase("sa")){
						if(p.hasPermission("witherarena.admin.selectarena")){
							if(args.length == 2){
								plugin.getArenaManager().selectArena(args[1], p);
							}else{
								p.sendMessage(ChatColor.RED + "Usage: /witherarena selectarena <arena>");
							}
						}else{
							p.sendMessage(noPerm);
						}
						
					}
					
					else if(args[0].equalsIgnoreCase("create")){
						
						if(args.length == 3){
							
							if(args[1].equalsIgnoreCase("arena")){
								if(p.hasPermission("witherarena.admin.create.arena")){
									
									plugin.getArenaManager().createNewArena(p, args[2]);
									
								}else{
									p.sendMessage(noPerm);
								}
							}else if(args[1].equalsIgnoreCase("class")){
								
								if(p.hasPermission("witherarena.admin.create.class")){
									
									plugin.getClassManager().createNewClass(p, args[2]);
									
								}else{
									p.sendMessage(noPerm);
								}
								
								
							}else{
								p.sendMessage(ChatColor.RED + "Usage: /witherarena create <arena | class> <name>");
							}
							
						}else{
							p.sendMessage(ChatColor.RED + "Usage: /witherarena create <arena | class> <name>");
						}
						
					}
					
					else if(args[0].equalsIgnoreCase("delete")){
						
						if(args.length == 3){
							
							if(args[1].equalsIgnoreCase("arena")){
								if(p.hasPermission("witherarena.admin.delete.arena")){
									
									plugin.getArenaManager().deleteArena(p, args[2]);
									
									
								}else{
									p.sendMessage(noPerm);
								}
							}else if(args[1].equalsIgnoreCase("class")){
								
								if(p.hasPermission("witherarena.admin.delete.class")){
									
									plugin.getClassManager().deleteClass(p, args[2]);
									
									
								}else{
									p.sendMessage(noPerm);
								}
								
								
								
							}else{
								p.sendMessage(ChatColor.RED + "Usage: /witherarena create <arena | class> <name>");
							}
							
						}else{
							p.sendMessage(ChatColor.RED + "Usage: /witherarena create <arena | class> <name>");
						}
						
					}
					
					
					else if(args[0].equalsIgnoreCase("set")){
						 
						 if(args.length == 2){
								if(args[1].equalsIgnoreCase("spawn")){
									sender.sendMessage(ChatColor.RED + "Usage: /witherarena set spawn <wither | zombie>");
								}else if(args[1].equalsIgnoreCase("region")){
									sender.sendMessage(ChatColor.RED + "Usage: /witherarena set region <region_name>");
								}else if(args[1].equalsIgnoreCase("warp")){
									sender.sendMessage(ChatColor.RED + "Usage: /witherarena set warp <lobby | arena | spectator>");
								}else{
									 p.sendMessage(ChatColor.RED + "Usage: /witherarena set <warp | spawn | region>");
								 }
								 
						 }else if(args.length == 3){
							 
							 if(args[1].equalsIgnoreCase("spawn")){
								 if(args[2].equalsIgnoreCase("wither")){
									 plugin.getArenaManager().setWitherSpawn(p.getLocation(), p);
								 }else if(args[2].equalsIgnoreCase("zombie")){
									 sender.sendMessage(ChatColor.RED + "Usage: /witherarena set spawn zombie <spawn_name>");
								 }else{
									 sender.sendMessage(ChatColor.RED + "Usage: /witherarena set spawn <wither | zombie>");
								 }
							 }else if(args[1].equalsIgnoreCase("warp")){
								 if(args[2].equalsIgnoreCase("lobby")){
										
										plugin.getArenaManager().setLobbyWarp(p.getLocation(), p);
										
									}else if(args[2].equalsIgnoreCase("arena")){
										
										plugin.getArenaManager().setArenaWarp(p.getLocation(), p);
										
									}else if(args[2].equalsIgnoreCase("spectator")){
										
										plugin.getArenaManager().setSpecWarp(p.getLocation(), p);
										
									}else{
										sender.sendMessage(ChatColor.RED + "Usage: /witherarena set warp <lobby | arena | spectator>");
									}
							 }else if(args[1].equalsIgnoreCase("region")){
								 plugin.getArenaManager().setRegion(args[2], p);
							 }else{
								 p.sendMessage(ChatColor.RED + "Usage: /witherarena set <warp | spawn | region>");
							 }
						 }else if(args.length == 4){
							 if(args[1].equalsIgnoreCase("spawn")){
								 if(args[2].equalsIgnoreCase("zombie")){
									 plugin.getZombieManager().addZombieSpawn(args[3], p.getLocation(), p);
								 }else{
									 sender.sendMessage(ChatColor.RED + "Usage: /witherarena set spawn <wither | zombie>");
								 }
							 }else{
								 p.sendMessage(ChatColor.RED + "Usage: /witherarena set <warp | spawn | region>");
							 }
						 }else{
							 p.sendMessage(ChatColor.RED + "Usage: /witherarena set <warp | spawn | region>");
						 }
						 
					 }else{
						 sender.sendMessage(ChatColor.GREEN + "WitherArena v"+plugin.getDescription().getVersion());
					 }
							

							
					
					
				}else{
					
					sender.sendMessage(ChatColor.GREEN + "WitherArena v"+plugin.getDescription().getVersion());
					
				}
				
			}else{
				
				sender.sendMessage(plugin.prefix + "Error: You are not a player");
				
			}
			
			
		}
		return true;
	}
	
	
	







	
	
	
	public static String getFinalArg(final String[] args, final int start)
	{
		final StringBuilder bldr = new StringBuilder();
		for (int i = start; i < args.length; i++)
		{
			if (i != start)
			{
				bldr.append(" ");
			}
			bldr.append(args[i]);
		}
		return bldr.toString();
	}

}
