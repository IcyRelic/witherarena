package me.icyrelic.com.Manager.Class;

import java.util.ArrayList;
import java.util.Set;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;

import me.icyrelic.com.WitherArena;


public class ClassManager {
	
	WitherArena plugin;
	public ClassManager(WitherArena instance) {

		plugin = instance;

		}
	
	public void loadClasses(){
		
		
		if(plugin.getConfig().contains("classes")){
			Set<String> classes = plugin.getConfig().getConfigurationSection("classes").getKeys(false);
			
			String classesstr = classes.toString().replace("[", "").replace("]", "");
			String[] str = classesstr.split(", ");
			
			int x = 0;
			while (x < str.length){
				
				getClass(str[x]).addClass(plugin.getConfig().getBoolean("classes."+str[x]+".use_permission"));
				x++;
			}
			
		}
		
	}
	
	public ArrayList<String> getClasses(){
		return getClass("").list();
	}
	
	public final Class getClass(String className){
		return new Class(className);
	}
	
	public void selectClass(Player p, String className, Sign sign){
		p.getInventory().clear();
        p.getInventory().setArmorContents(null);
        
        delayAssignClass(p, className, sign);
        
		
	}
	
	public void deleteClass(Player p, String className){


		if(getClass(className).exists()){
				getClass(className).deleteClass();
				
				plugin.getConfig().set("classes."+className, null);
				plugin.saveConfig();
				
				p.sendMessage(plugin.prefix + "Class Deleted!");
		}else{
			p.sendMessage(plugin.prefix + ChatColor.RED + "Error: That class doesnt exist");
		}
		
		
	}
	
	public boolean createNewClass(Player p, String className){
		
		if(!getClass(className).exists()){
			getClass(className).addClass(plugin.getConfig().getBoolean("classes."+className+".use_permission"));
			plugin.getConfig().set("classes."+className+".use_permission", false);
			plugin.saveConfig();
			
			p.sendMessage(plugin.prefix + "Class Created!");
			return true;
		}else{
			p.sendMessage(plugin.prefix + ChatColor.RED + "Error: That class already exist!");
			return false;
		}
		
	}
	public void listClasses(Player p){
		if(plugin.getConfig().contains("classes")){
			p.sendMessage(plugin.prefix + ChatColor.GREEN + "Classes: " + ChatColor.GRAY +getClasses().toString().replace("]", "").replace("[", ""));
			
		}else{
			p.sendMessage(plugin.prefix + ChatColor.GREEN + "Classes: " + ChatColor.GRAY +"No classes to list");
		}
	}
	
	private Block findChestBelow(Block b, int left) {
        if (left < 0) return null;
        
        if (b.getType() == Material.CHEST || b.getType() == Material.TRAPPED_CHEST) {
            return b;
        }
        return findChestBelow(b.getRelative(BlockFace.DOWN), left - 1);
    }
	
	
	private void delayAssignClass(final Player p, final String className, final Sign sign) {
        plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin,new Runnable() {
            @SuppressWarnings("deprecation")
			public void run() {
            	
            	
            	BlockFace backwards = ((org.bukkit.material.Sign) sign.getData()).getFacing().getOppositeFace();
                Block blockSign   = sign.getBlock();
                Block blockBelow  = blockSign.getRelative(BlockFace.DOWN);
                Block blockBehind = blockBelow.getRelative(backwards);
        		
                
        		
        		 Block blockChest = findChestBelow(blockBelow, 6);
                 
        		 
        		 
                 // Then, if no chest was found, check the pillar behind the sign
                 if (blockChest == null) blockChest = findChestBelow(blockBehind, 6);
                 
                 // If a chest was found, get the contents
                 if (blockChest != null) {
                     InventoryHolder holder = (InventoryHolder) blockChest.getState();
                     ItemStack[] contents = holder.getInventory().getContents();
                     // Guard against double-chests for now
                     if (contents.length > 36) {
                         ItemStack[] newContents = new ItemStack[36];
                         for (int i = 0; i < 36; i++) {
                        		 newContents[i] = contents[i];
                        	 
                            
                             
                         }
                         
                         
                         
                         contents = newContents;
                         
                     }
                     
                     p.getInventory().setContents(contents);
                     
                     p.updateInventory();
                     
                 }
            	
            	
            }
        });
 }

}
