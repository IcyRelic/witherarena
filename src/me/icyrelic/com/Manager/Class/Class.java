package me.icyrelic.com.Manager.Class;

import java.util.ArrayList;
import java.util.HashMap;

public class Class {

	private static String className = "";
	private static ArrayList<String> classNames = new ArrayList<String>();
	private static HashMap<String, Boolean> classPermissions = new HashMap<String, Boolean>();
	
	public Class(String classname) {
		className = classname;
	}
	
	public ArrayList<String> list(){
		return classNames;
	}
	public boolean addClass(boolean usePermission){
		if(classNames.contains(className)){
			return false;
		}else{
			classNames.add(className);
			classPermissions.put(className, usePermission);
			return true;
		}
		
	}
	
	public boolean needsPermission(){
		return classPermissions.get(className);
	}
	
	public boolean deleteClass(){
		if(classNames.contains(className)){
			classNames.remove(className);
			return true;
		}else{
			return false;
		}
		
	}
	
	public boolean exists(){
		if(classNames.contains(className)){
			return true;
		}else{
			return false;
		}
		
	}
}
