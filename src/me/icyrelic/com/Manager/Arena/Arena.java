package me.icyrelic.com.Manager.Arena;

import java.util.ArrayList;
import java.util.HashMap;

import me.icyrelic.com.WitherArena;

public class Arena {
	
	private static HashMap<String, Boolean> arenas = new HashMap<String, Boolean>();
	private static ArrayList<String> arenaNames = new ArrayList<String>();
	private static HashMap<Integer, String> withers = new HashMap<Integer, String>();
	private static HashMap<String, Integer> withersID = new HashMap<String, Integer>();
	
	private static ArrayList<String> zombies = new ArrayList<String>();
	
	private static HashMap<String, Boolean> hasZS = new HashMap<String, Boolean>();
	
	WitherArena plugin;
	
	private static String arenaName = "";
	
	public Arena(String arena, WitherArena instance) {
		arenaName = arena;
		plugin = instance;
	}

	public boolean addArena(){
		
		if(!arenas.containsKey(arenaName)){
			arenas.put(arenaName, false);
			arenaNames.add(arenaName);
			return true;
		}else{
			return false;
		}
		
	}
	
	public boolean hasZombieSpawns(){
		if(hasZS.containsKey(arenaName)){
			return hasZS.get(arenaName);
		}else{
			return false;
		}
		
	}
	
	public boolean setHasZombieSpawns(boolean hasSpawns){
		if(hasZS.containsKey(arenaName)){
			hasZS.remove(arenaName);
		}
		hasZS.put(arenaName, hasSpawns);
		return true;
	}
	
	public ArrayList<String> getPlayers(){
		ArrayList<String> players = plugin.getPlayerManager().getPlayers();
		ArrayList<String> playersInArena = new ArrayList<String>();
		
		int x = 0;
		
		while(x < players.size()){
			if(plugin.getPlayerManager().getPlayer(players.get(x)).getPlayerArena().equals(arenaName)){
				playersInArena.add(players.get(x));
			}
			x++;
		}
		
		return playersInArena;
		
	}
	
	
	public ArrayList<String> list(){
		return arenaNames;
	}
	
	public boolean isRunning(){

		if(arenas.containsKey(arenaName)){
			if(arenas.get(arenaName)){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
		
	}
	
	public boolean setRunning(Boolean running){

		if(arenas.containsKey(arenaName)){
			arenas.remove(arenaName);
			arenas.put(arenaName, running);
			return true;
		}else{
			return false;
		}
		
	}
	
	public boolean deleteArena(){

		if(arenas.containsKey(arenaName)){
			arenas.remove(arenaName);
			arenaNames.remove(arenaName);
			return true;
		}else{
			return false;
		}
		
	}
	
	public int getWitherID(){
		
		if(isRunning()){
			return withersID.get(arenaName);
		}else{
			return 0;
		}
		
	}
	
	public boolean hasWither(){
		
		if(withersID.containsKey(arenaName)){
			return true;
		}else{
			return false;
		}
		
	}
	
	public boolean removeWither(){
		
		if(withersID.containsKey(arenaName)){
			withers.remove(withersID.get(arenaName));
			withersID.remove(arenaName);
			return true;
		}else{
			return false;
		}
		
	}
	
	public boolean entityHasArena(int entityid){
		
		if(withers.containsKey(entityid)){
			return true;
		}else{
			return false;
		}
		
	}
	
	public ArrayList<String> getZombies(){
		
		ArrayList<String> arenaZombies = new ArrayList<String>();
		
		arenaZombies.clear();
		
		int y = 0;
		while (y < zombies.size()){
			String[] str = zombies.get(y).split("_");
			if(zombies.get(y).contains(str[0])){
				
				arenaZombies.add(zombies.get(y));
			}

			y++;
			
		}
		
		return arenaZombies;
		
	}
	
	public boolean removeZombie(int entityid){
		zombies.remove(arenaName+"_"+entityid);
		return true;
	}
	
	public boolean addZombie(int entityid){
		
		if(zombies.contains(arenaName+"_"+entityid)){
			zombies.remove(arenaName+"_"+entityid);
		}
		zombies.add(arenaName+"_"+entityid);
		return true;
	}
	
	public boolean addWither(int entityid){
		
		if(withersID.containsKey(arenaName)){
			withers.remove(withersID.get(arenaName));
			withersID.remove(arenaName);
		}
		
		withers.put(entityid, arenaName);
		withersID.put(arenaName, entityid);
		return true;
		
	}
	
	public String getWithersArena(int entityid){
		
		if(withers.containsKey(entityid)){
			return withers.get(entityid);
		}else{
			return "Error: Wither not found";
		}
		
	}
	
	public boolean exists(){
		if(arenas.containsKey(arenaName)){
			return true;
		}else{
			return false;
		}
		
	}

}
