package me.icyrelic.com.Manager.Arena;


import java.util.ArrayList;
import java.util.Set;

import me.icyrelic.com.WitherArena;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitTask;

import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.flags.DefaultFlag;
import com.sk89q.worldguard.protection.flags.StateFlag.State;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class ArenaManager {
	
	WitherArena plugin;
	public ArenaManager(WitherArena instance) {

		plugin = instance;

	}
	
	String noSelected = (ChatColor.RED + "Error: No arena selected!");
	
	public boolean spawnZombies(String arena, boolean spawn){
		@SuppressWarnings("unused")
		BukkitTask task = new Zombies(arena, plugin).runTaskTimer(plugin, 300, 1800);
		return true;
	}
	
	public ArrayList<String> getArenas(){
		return getArena("").list();
	}
	
	public final Arena getArena(String arena){
		return new Arena(arena, plugin);
	}
	
	
 	public void tryJoinDefault(Player p){
		if(getArenas().size() == 1){
			addPlayer(p, getArenas().get(0));
		}else if(getArenas().contains("default")){
			addPlayer(p, "default");
		}else{
			p.sendMessage(ChatColor.RED + "Usage: /witherarena join <arena>");
		}
		
	}
	
	public void loadArenas(){
		
		
		if(plugin.getConfig().contains("arenas")){
			Set<String> arenas = plugin.getConfig().getConfigurationSection("arenas").getKeys(false);
			
			String arenastostr = arenas.toString().replace("[", "").replace("]", "");
			String[] str = arenastostr.split(", ");
			
			int x = 0;
			while (x < str.length){
				
				getArena(str[x]).addArena();
				x++;
			}
			
		}
		
	}
	
	public void Create(String arena, Location lobbyWarp, Location arenaWarp,
			Location specWarp, Location witherSpawn, String wgRegion, String lobbyworld, String arenaworld,
			String specworld, String witherworld, float lobbypitch, float lobbyyaw, float arenapitch, float arenayaw
			, float specpitch, float specyaw, float witherpitch, float witheryaw, ArrayList<String> zombieSpawns) {
		
		String lobbystr = (lobbyWarp.getBlockX()+", "+lobbyWarp.getBlockY()+", "+lobbyWarp.getBlockZ());
		String arenastr = (arenaWarp.getBlockX()+", "+arenaWarp.getBlockY()+", "+arenaWarp.getBlockZ());
		String specstr = (specWarp.getBlockX()+", "+specWarp.getBlockY()+", "+specWarp.getBlockZ());
		String witherstr = (witherSpawn.getBlockX()+", "+witherSpawn.getBlockY()+", "+witherSpawn.getBlockZ());
		

		
		plugin.getConfig().set("arenas."+arena+".warps.lobby.location", lobbystr);
		plugin.getConfig().set("arenas."+arena+".warps.lobby.pitch", lobbypitch);
		plugin.getConfig().set("arenas."+arena+".warps.lobby.yaw", lobbyyaw);
		plugin.getConfig().set("arenas."+arena+".warps.lobby.world", lobbyworld);
		
		plugin.getConfig().set("arenas."+arena+".warps.arena.location", arenastr);
		plugin.getConfig().set("arenas."+arena+".warps.arena.pitch", arenapitch);
		plugin.getConfig().set("arenas."+arena+".warps.arena.yaw", arenayaw);
		plugin.getConfig().set("arenas."+arena+".warps.arena.world", arenaworld);
		
		plugin.getConfig().set("arenas."+arena+".warps.spectator.location", specstr);
		plugin.getConfig().set("arenas."+arena+".warps.spectator.pitch", specpitch);
		plugin.getConfig().set("arenas."+arena+".warps.spectator.yaw", specyaw);
		plugin.getConfig().set("arenas."+arena+".warps.spectator.world", specworld);
		
		plugin.getConfig().set("arenas."+arena+".spawns.wither.location", witherstr);
		plugin.getConfig().set("arenas."+arena+".spawns.wither.pitch", witherpitch);
		plugin.getConfig().set("arenas."+arena+".spawns.wither.yaw", witheryaw);
		plugin.getConfig().set("arenas."+arena+".spawns.wither.world", witherworld);
		
		int x = 0;
		
		while(x < zombieSpawns.size()){
			
			String[] str = zombieSpawns.get(x).split("_");
			
			String spawnName = str[1];
			
			Location spawnLoc = (Location) plugin.getSelectedArena().getSpawn(spawnName, arena).get(0);
			String world = (String) plugin.getSelectedArena().getSpawn(spawnName, arena).get(1);
			
			float pitch = (Float) plugin.getSelectedArena().getSpawn(spawnName, arena).get(2);
			float yaw = (Float) plugin.getSelectedArena().getSpawn(spawnName, arena).get(3);
			
			String spawnstr = (spawnLoc.getBlockX()+", "+spawnLoc.getBlockY()+", "+spawnLoc.getBlockZ());
			
			plugin.getConfig().set("arenas."+arena+".spawns.zombie."+spawnName+".location", spawnstr);
			plugin.getConfig().set("arenas."+arena+".spawns.zombie."+spawnName+".pitch", pitch);
			plugin.getConfig().set("arenas."+arena+".spawns.zombie."+spawnName+".yaw", yaw);
			plugin.getConfig().set("arenas."+arena+".spawns.zombie."+spawnName+".world", world);
			
			x++;
		}
		
		
		plugin.getConfig().set("arenas."+arena+".region", wgRegion);
		
		setupRegion(wgRegion, plugin.getServer().getWorld(witherworld));
		
		
		plugin.saveConfig();
		
		plugin.getSelectedArena().clear(arena);
		
		getArena(arena).setRunning(false);
		
		getArena(arena).addArena();
		
		
	}
	
	public void selectArena(String arena, Player p){
		
		if(plugin.getConfig().contains("arenas")){
			Set<String> arenas = plugin.getConfig().getConfigurationSection("arenas").getKeys(false);
			
			
			if(arenas.contains(arena)){
				
				plugin.getSelectedArena().setSelectedArena(p.getName(), arena);
				p.sendMessage(plugin.prefix + "Selected Arena: " + arena);
				
			}else{
				
				p.sendMessage(plugin.prefix + ChatColor.RED + "Error: That Arena Doesnt Exist");
				
			}
			
		}else{
			
			p.sendMessage(plugin.prefix + ChatColor.RED + "Error: That Arena Doesnt Exist");
			
		}
		
	}
	
	public void checkData(Player p){
		
		if(plugin.getSelectedArena().hasSelectedArena(p.getName())){
			String arena = plugin.getSelectedArena().getSelectedArena(p.getName());
			
			if(plugin.getConfig().contains("arenas")){
				Set<String> keys = plugin.getConfig().getConfigurationSection("arenas").getKeys(false);
				if(!keys.contains(arena)){
					
					p.sendMessage(ChatColor.GOLD + "Data Needed To Complete Creation of arena: "+ChatColor.YELLOW + arena);
					p.sendMessage(ChatColor.GOLD + "Warps: "+ChatColor.YELLOW + plugin.getSelectedArena().checkWarps(arena));
					p.sendMessage(ChatColor.GOLD + "Spawns: "+ChatColor.YELLOW + plugin.getSelectedArena().checkSpawn(arena));
					p.sendMessage(ChatColor.GOLD + "Regions: "+ChatColor.YELLOW + plugin.getSelectedArena().checkRegion(arena));
					
				}else{
					
					p.sendMessage(ChatColor.GREEN + "This arena is setup and ready to use!");
					
				}
			}else{
				p.sendMessage(ChatColor.GOLD + "Data Needed To Complete Creation of arena: "+ChatColor.YELLOW + arena);
				p.sendMessage(ChatColor.GOLD + "Warps: "+ChatColor.YELLOW + plugin.getSelectedArena().checkWarps(arena));
				p.sendMessage(ChatColor.GOLD + "Spawns: "+ChatColor.YELLOW + plugin.getSelectedArena().checkSpawn(arena));
				p.sendMessage(ChatColor.GOLD + "Regions: "+ChatColor.YELLOW + plugin.getSelectedArena().checkRegion(arena));
			}
				
			}else{
				
				p.sendMessage(plugin.prefix+noSelected);
				
			}
	}
	
	public void spectateArena(Player p, String arena){
		
		String spec = plugin.getConfig().getString("arenas."+arena+".warps.spectator.location");
		String specworld = plugin.getConfig().getString("arenas."+arena+".warps.spectator.world");
		float pitch = plugin.getConfig().getInt("arenas."+arena+".warps.spectator.pitch");
		float yaw = plugin.getConfig().getInt("arenas."+arena+".warps.spectator.yaw");
		String[] str = spec.split(", ");
		
		World w = plugin.getServer().getWorld(specworld);
		
		Location loc = new Location(w, Integer.parseInt(str[0]), Integer.parseInt(str[1]), Integer.parseInt(str[2]));
		
		loc.setPitch(pitch);
        loc.setYaw(yaw);
        loc.add(0.5, 0, 0.5);
		
		p.teleport(loc);
		
	}

	public void teleportToArena(Player p, String arena){
		String arena1 = plugin.getConfig().getString("arenas."+arena+".warps.arena.location");
		String arenaworld = plugin.getConfig().getString("arenas."+arena+".warps.arena.world");
		float pitch = plugin.getConfig().getInt("arenas."+arena+".warps.arena.pitch");
		float yaw = plugin.getConfig().getInt("arenas."+arena+".warps.arena.yaw");
		String[] str = arena1.split(", ");
		
		World w = plugin.getServer().getWorld(arenaworld);
		
		Location loc = new Location(w, Integer.parseInt(str[0]), Integer.parseInt(str[1]), Integer.parseInt(str[2]));
		
		loc.setPitch(pitch);
        loc.setYaw(yaw);
        loc.add(0.5, 0, 0.5);
		
		p.teleport(loc);
	}
	
	private boolean checkIfAllReady(String arena){
		
		
		
		int x = 0;
		while(x < plugin.getPlayerManager().getPlayer("").list().size()){
			Player p = plugin.getServer().getPlayer(plugin.getPlayerManager().getPlayer("").list().get(x));
			String playerArena = plugin.getPlayerManager().getPlayer(p.getName()).getPlayerArena();
			boolean ready = plugin.getPlayerManager().getPlayer(p.getName()).isReady();
			
			
			if(playerArena.equals(arena)){
				
				if(!ready){
					return false;
				}
				
			}
			
			x++;
			
		}
		
		return true;
		
	}
	
	public void spawnWither(String arena){
		
		String wither = plugin.getConfig().getString("arenas."+arena+".spawns.wither.location");
		String witherworld = plugin.getConfig().getString("arenas."+arena+".spawns.wither.world");
		float pitch = plugin.getConfig().getInt("arenas."+arena+".spawns.wither.pitch");
		float yaw = plugin.getConfig().getInt("arenas."+arena+".spawns.wither.yaw");
		String[] str = wither.split(", ");
		
		World w = plugin.getServer().getWorld(witherworld);
		Location loc = new Location(w, Integer.parseInt(str[0]), Integer.parseInt(str[1]), Integer.parseInt(str[2]));
		
		loc.setPitch(pitch);
        loc.setYaw(yaw);
        loc.add(0.5, 0, 0.5);
        
        int x = 0;
        while(x < plugin.getPlayerManager().getPlayer("").list().size()){
        	Player p = plugin.getServer().getPlayer(plugin.getPlayerManager().getPlayer("").list().get(x));
        	
        	p.sendMessage(plugin.prefix + "The Wither Has Spawned!!!");
        	
        	x++;
        }
        
        Entity entity = w.spawnEntity(loc, EntityType.PIG);
        int entityID = entity.getEntityId();
        
		getArena(arena).addWither(entityID);
	}
	
	public void endArena(String arena, World w){

		int y = 0;
		while(y < w.getEntities().size()){
			Entity e = w.getEntities().get(y);
			
			if(getArena(arena).hasWither()){
				if(getArena(arena).getWithersArena(e.getEntityId()).equals(arena)){
					e.remove();
					
					getArena(arena).removeWither();
					
				}
			}
			
			if(e.getType() == EntityType.ZOMBIE){
				
				int x = 0;
				
				while(x < plugin.getArenaManager().getArena("").getZombies().size()){
					String[] str = plugin.getArenaManager().getArena("").getZombies().get(x).split("_");
					String arena1 = str[0];
					String id = str[1];
					
					if(e.getEntityId() == Integer.parseInt(id)){
						plugin.getArenaManager().getArena(arena1).removeZombie(Integer.parseInt(id));
						e.remove();
					}
					x++;
				}
				
			}
			
			y++;
		}
		
		
		while(!plugin.getPlayerManager().getPlayer("").list().isEmpty()){
			Player p = plugin.getServer().getPlayer(plugin.getPlayerManager().getPlayer("").list().get(0));
			removePlayer(p, false, arena);
			teleportPlayerOut(p, arena);
			p.sendMessage(plugin.prefix + "The wither has died! You will be teleported out of the arena in 15 seconds!");
		}
		

		getArena(arena).setRunning(false);
		
	}
	
	public void removeWither(String arena, World w){

		int y = 0;
		while(y < w.getEntities().size()){
			Entity e = w.getEntities().get(y);
			
			if(getArena(arena).hasWither()){
				if(getArena(arena).getWithersArena(e.getEntityId()).equals(arena)){
					e.remove();
					
					getArena(arena).removeWither();
					
				}
			}
			
			y++;
		}
		
	}

	
	public void removeZombies(String arena, World w){

		int y = 0;
		while(y < w.getEntities().size()){
			Entity e = w.getEntities().get(y);
			
			
			if(e.getType() == EntityType.ZOMBIE){
				
				int x = 0;
				
				while(x < plugin.getArenaManager().getArena("").getZombies().size()){
					String[] str = plugin.getArenaManager().getArena("").getZombies().get(x).split("_");
					String arena1 = str[0];
					String id = str[1];
					
					if(e.getEntityId() == Integer.parseInt(id)){
						plugin.getArenaManager().getArena(arena1).removeZombie(Integer.parseInt(id));
						e.remove();
					}
					x++;
				}
				
			}
			
			
			y++;
		}
		
	}
	public boolean checkEmptyArena(String arena){
		int players = 0;
		int x = 0;
		while(x < plugin.getPlayerManager().getPlayer("").list().size()){
			Player p = plugin.getServer().getPlayer(plugin.getPlayerManager().getPlayer("").list().get(x));
			if(plugin.getPlayerManager().getPlayer(p.getName()).isInArena()){
				if(plugin.getPlayerManager().getPlayer(p.getName()).getPlayerArena().equals(arena)){
					players++;
				}
			}
			x++;
		}
		
		if(players == 0){
			return true;
		}else{
			return false;
		}
	}
	
	public int getArenaPlayerCount(String arena){
		int players = 0;
		int x = 0;
		while(x < plugin.getPlayerManager().getPlayer("").list().size()){
			Player p = plugin.getServer().getPlayer(plugin.getPlayerManager().getPlayer("").list().get(x));
			if(plugin.getPlayerManager().getPlayer(p.getName()).isInArena()){
				if(plugin.getPlayerManager().getPlayer(p.getName()).getPlayerArena().equals(arena)){
					players++;
				}
			}
		}
		
		return players;
	}
	
	public void startArena(String arena){
		
		int x = 0;
		while(x < plugin.getPlayerManager().getPlayer("").list().size()){
			Player p = plugin.getServer().getPlayer(plugin.getPlayerManager().getPlayer("").list().get(x));
			if(plugin.getPlayerManager().getPlayer(p.getName()).isInArena()){
				if(plugin.getPlayerManager().getPlayer(p.getName()).getPlayerArena().equals(arena)){
					p.sendMessage(plugin.prefix + "The Arena Has Started!");
					teleportToArena(p, arena);
					plugin.getPlayerManager().getPlayer(p.getName()).removeFromLobby();
				}
			}
			x++;
		}
		
		
		
		
		getArena(arena).setRunning(true);
		spawnWither(arena);
		spawnZombies(arena, true);
	}
	
	public void addPlayer(Player p, String arena){
		
		if(!plugin.getPlayerManager().getPlayer(p.getName()).isInArena()){
			if(getArena(arena).exists()){
				if(checkEmptyInventory(p) && checkEmptyArmor(p)){
					
					if(!getArena(arena).isRunning()){
						p.sendMessage(plugin.prefix + "Joined Arena: "+arena);
						
						plugin.getPlayerManager().getPlayer(p.getName()).addPlayer(arena);
						
						String lobby = plugin.getConfig().getString("arenas."+arena+".warps.lobby.location");
						String lobbyworld = plugin.getConfig().getString("arenas."+arena+".warps.lobby.world");
						float pitch = plugin.getConfig().getInt("arenas."+arena+".warps.lobby.pitch");
						float yaw = plugin.getConfig().getInt("arenas."+arena+".warps.lobby.yaw");
						String[] str = lobby.split(", ");
						
						World w = plugin.getServer().getWorld(lobbyworld);
						
						Location loc = new Location(w, Integer.parseInt(str[0]), Integer.parseInt(str[1]), Integer.parseInt(str[2]));
						
						loc.setPitch(pitch);
		                loc.setYaw(yaw);
		                loc.add(0.5, 0, 0.5);
						
						p.teleport(loc);
						p.setHealth(20);
						p.setFoodLevel(20);
						
					}else{
						p.sendMessage(plugin.prefix + ChatColor.RED + "Arena currently in progress");
					}
					
				}else{
					p.sendMessage(plugin.prefix + ChatColor.RED + "You must have an empty inventory!");
				}
			}else{
				p.sendMessage(plugin.prefix + ChatColor.RED + "Error: Arena not found");
			}
			
		}else{
			p.sendMessage(plugin.prefix + ChatColor.RED + "Error: You are currently in an arena!");
		}



		
		
	}
	
	public void deleteArena(Player p, String arena){


		if(getArena(arena).exists()){
			if(!getArena(arena).isRunning()){
				plugin.getSelectedArena().clear(arena);
				getArena(arena).deleteArena();
				
				plugin.getConfig().set("arenas."+arena, null);
				plugin.saveConfig();
				
				p.sendMessage(plugin.prefix + "Arena Deleted!");
			}else{
				p.sendMessage(plugin.prefix + ChatColor.RED + "Error: Arena Currently In Progress");
			}
		}else{
			p.sendMessage(plugin.prefix + ChatColor.RED + "Error: That Arena Doesnt Exist");
		}
		
		
	}
	
	public void removePlayer(Player p, boolean tp, String arena){

		if(plugin.getPlayerManager().getPlayer(p.getName()).isInArena()){
			
			if(tp){
				spectateArena(p, plugin.getPlayerManager().getPlayer(p.getName()).getPlayerArena());
			}
			
			plugin.getPlayerManager().getPlayer(p.getName()).removeFromArena();
			
			p.getInventory().clear();
			p.getInventory().setArmorContents(null);
			
			p.sendMessage(plugin.prefix + "You have left the arena");
			if(getArena(arena).isRunning()){
				if(checkEmptyArena(arena)){
					endArena(arena, p.getLocation().getWorld());
				}
			}
			
			//Gives rewards
			
			
		}else{
			p.sendMessage(plugin.prefix + ChatColor.RED + "Error: You are not in an arena!");
		}
	}

	public void teleportPlayerOut(final Player p, final String arena){
		int delays = 15;
		plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin,new Runnable() {
		  public void run() {
			  
			  spectateArena(p, arena);
			  p.sendMessage(plugin.prefix + "Thanks for playing!");
			  
		  }
		}, 20*delays);}
	
	public void checkCreate(String arena, Player p){
		if(plugin.getSelectedArena().canCreate(arena)){
			
			
			
			Location lobbyWarp = (Location) plugin.getSelectedArena().getLobby(arena).get(0);
			Location arenaWarp = (Location) plugin.getSelectedArena().getArena(arena).get(0);
			Location specWarp = (Location) plugin.getSelectedArena().getSpec(arena).get(0);
			Location witherSpawn = (Location) plugin.getSelectedArena().getWitherSpawn(arena).get(0);
			
			String wgRegion = plugin.getSelectedArena().getRegion(arena);
			String lobbyworld = (String) plugin.getSelectedArena().getLobby(arena).get(1);
			String arenaworld = (String) plugin.getSelectedArena().getArena(arena).get(1);
			String specworld = (String) plugin.getSelectedArena().getSpec(arena).get(1);
			String witherworld = (String) plugin.getSelectedArena().getWitherSpawn(arena).get(1);
			ArrayList<String> zombieSpawns = plugin.getSelectedArena().zombieSpawns(arena);
			
			
			float lobbypitch = (Float) plugin.getSelectedArena().getLobby(arena).get(2);
			float lobbyyaw = (Float) plugin.getSelectedArena().getLobby(arena).get(3);
			
			float arenapitch = (Float) plugin.getSelectedArena().getArena(arena).get(2);
			float arenayaw = (Float) plugin.getSelectedArena().getArena(arena).get(3);
			
			float specpitch = (Float) plugin.getSelectedArena().getSpec(arena).get(2);
			float specyaw = (Float) plugin.getSelectedArena().getSpec(arena).get(3);
			
			float witherpitch = (Float) plugin.getSelectedArena().getWitherSpawn(arena).get(2);
			float witheryaw = (Float) plugin.getSelectedArena().getWitherSpawn(arena).get(3);
			
			p.sendMessage(plugin.prefix + "Arena: " + arena + " has been created!");
			Create(arena, lobbyWarp, arenaWarp, specWarp, witherSpawn, wgRegion, lobbyworld, arenaworld, specworld, witherworld,
					lobbypitch, lobbyyaw, arenapitch, arenayaw, specpitch, specyaw, witherpitch, witheryaw, zombieSpawns);
		}
	}
	

	
	public void setLobbyWarp(Location loc, Player p){
		
		if(plugin.getSelectedArena().hasSelectedArena(p.getName())){
			String arena = plugin.getSelectedArena().getSelectedArena(p.getName());
			
			if(checkAlreadyCreated(arena)){
				editLobbyWarp(p);
			}else{
				plugin.getSelectedArena().setLobby(arena, loc, p.getWorld().getName(), loc.getPitch(), loc.getYaw());
				p.sendMessage(plugin.prefix + "Lobby Warp Set For Arena: " + arena);

				checkCreate(arena, p);
			}
			
			

			
		}else{
			p.sendMessage(plugin.prefix+noSelected);
		}
		
	}
	
	public void setArenaWarp(Location loc, Player p){
		
		if(plugin.getSelectedArena().hasSelectedArena(p.getName())){
			String arena = plugin.getSelectedArena().getSelectedArena(p.getName());
			
			if(checkAlreadyCreated(arena)){
				editArenaWarp(p);
			}else{
				plugin.getSelectedArena().setArena(arena, loc, p.getWorld().getName(), loc.getPitch(), loc.getYaw());
				p.sendMessage(plugin.prefix + "Arena Warp Set For Arena: " + arena);
				checkCreate(arena, p);
				
			}
			

		}else{
			p.sendMessage(plugin.prefix+noSelected);
		}
		
	}
	
	public void setSpecWarp(Location loc, Player p){
		if(plugin.getSelectedArena().hasSelectedArena(p.getName())){
			String arena = plugin.getSelectedArena().getSelectedArena(p.getName());
			
			if(checkAlreadyCreated(arena)){
				editSpecWarp(p);
			}else{
				plugin.getSelectedArena().setSpec(arena, loc, p.getWorld().getName(), loc.getPitch(), loc.getYaw());
			p.sendMessage(plugin.prefix + "Spectator Warp Set For Arena: " + arena);
			checkCreate(arena, p);
			}
			

			
		}else{
			p.sendMessage(plugin.prefix+noSelected);
		}
		
	}
	
	public void setWitherSpawn(Location loc, Player p){
		if(plugin.getSelectedArena().hasSelectedArena(p.getName())){
			String arena = plugin.getSelectedArena().getSelectedArena(p.getName());
			
			if(checkAlreadyCreated(arena)){
				editWitherSpawn(p);
			}else{
				plugin.getSelectedArena().witherSpawn(arena, loc, p.getWorld().getName(), loc.getPitch(), loc.getYaw());
				p.sendMessage(plugin.prefix + "Wither Spawn Set For Arena: " + arena);
				checkCreate(arena, p);
				
			}
			
			

			
		}else{
			p.sendMessage(plugin.prefix+noSelected);
		}
	}
	
	public void setRegion(String region, Player p){
		if(plugin.getSelectedArena().hasSelectedArena(p.getName())){
			String arena = plugin.getSelectedArena().getSelectedArena(p.getName());
			
			
			if(checkAlreadyCreated(arena)){
				editRegion(region, p);
			}else{
				
				if(plugin.getSelectedArena().setRegion(arena, region, p.getWorld())){
					p.sendMessage(plugin.prefix + "World Guard Region Selected For Arena: " + arena);
					checkCreate(arena, p);
				}else{
					p.sendMessage(plugin.prefix + ChatColor.RED + "Invalid World Guard Region!");
				}
				
			}
			

			
		}else{
			p.sendMessage(plugin.prefix+noSelected);
		}
	}
	
	public void editLobbyWarp(Player p){
		if(plugin.getSelectedArena().hasSelectedArena(p.getName())){
			String arena = plugin.getSelectedArena().getSelectedArena(p.getName());
			String lobbystr = (p.getLocation().getBlockX()+", "+p.getLocation().getBlockY()+", "+p.getLocation().getBlockZ());
			
			
			plugin.getConfig().set("arenas."+arena+".warps.lobby.location", lobbystr);
			plugin.getConfig().set("arenas."+arena+".warps.lobby.pitch", p.getLocation().getPitch());
			plugin.getConfig().set("arenas."+arena+".warps.lobby.yaw", p.getLocation().getYaw());
			plugin.getConfig().set("arenas."+arena+".warps.lobby.world", p.getWorld().getName());
			
			plugin.saveConfig();
			p.sendMessage(plugin.prefix + "Lobby Warp Updated For Arena: " + arena);

			
		}else{
			p.sendMessage(plugin.prefix+noSelected);
		}
	}
	
	public void editArenaWarp(Player p){
		if(plugin.getSelectedArena().hasSelectedArena(p.getName())){
			String arena = plugin.getSelectedArena().getSelectedArena(p.getName());
			
			String arenastr = (p.getLocation().getBlockX()+", "+p.getLocation().getBlockY()+", "+p.getLocation().getBlockZ());
			
			
			plugin.getConfig().set("arenas."+arena+".warps.arena.location", arenastr);
			plugin.getConfig().set("arenas."+arena+".warps.arena.pitch", p.getLocation().getPitch());
			plugin.getConfig().set("arenas."+arena+".warps.arena.yaw", p.getLocation().getYaw());
			plugin.getConfig().set("arenas."+arena+".warps.arena.world", p.getWorld().getName());
			plugin.saveConfig();
			p.sendMessage(plugin.prefix + "Arena Warp Updated For Arena: " + arena);

			
		}else{
			p.sendMessage(plugin.prefix+noSelected);
		}
	}
	
	public void editSpecWarp(Player p){
		if(plugin.getSelectedArena().hasSelectedArena(p.getName())){
			String arena = plugin.getSelectedArena().getSelectedArena(p.getName());
			
			String spectatorstr = (p.getLocation().getBlockX()+", "+p.getLocation().getBlockY()+", "+p.getLocation().getBlockZ());
			
			
			plugin.getConfig().set("arenas."+arena+".warps.spectator.location", spectatorstr);
			plugin.getConfig().set("arenas."+arena+".warps.spectator.pitch", p.getLocation().getPitch());
			plugin.getConfig().set("arenas."+arena+".warps.spectator.yaw", p.getLocation().getYaw());
			plugin.getConfig().set("arenas."+arena+".warps.spectator.world", p.getWorld().getName());
			plugin.saveConfig();
			p.sendMessage(plugin.prefix + "Spectator Warp Updated For Arena: " + arena);

			
		}else{
			p.sendMessage(plugin.prefix+noSelected);
		}
	}
	
	public void editWitherSpawn( Player p){
		if(plugin.getSelectedArena().hasSelectedArena(p.getName())){
			String arena = plugin.getSelectedArena().getSelectedArena(p.getName());
			
			String witherstr = (p.getLocation().getBlockX()+", "+p.getLocation().getBlockY()+", "+p.getLocation().getBlockZ());
			
			
			plugin.getConfig().set("arenas."+arena+".spawns.wither.location", witherstr);
			plugin.getConfig().set("arenas."+arena+".spawns.wither.pitch", p.getLocation().getPitch());
			plugin.getConfig().set("arenas."+arena+".spawns.wither.yaw", p.getLocation().getYaw());
			plugin.getConfig().set("arenas."+arena+".spawns.wither.world", p.getWorld().getName());
			plugin.saveConfig();
			p.sendMessage(plugin.prefix + "Wither Spawn Updated For Arena: " + arena);

			
		}else{
			p.sendMessage(plugin.prefix+noSelected);
		}
	}
	
	public void editRegion(String region, Player p){
		if(plugin.getSelectedArena().hasSelectedArena(p.getName())){
			String arena = plugin.getSelectedArena().getSelectedArena(p.getName());
			
			boolean isRegion = getWorldGuard().getRegionManager(p.getWorld()).hasRegion(region);
			
			if(isRegion){
				
				plugin.getConfig().set("arenas."+arena+".region", region);
				plugin.saveConfig();
				
				p.sendMessage(plugin.prefix + "Region Updated For Arena: " + arena);
				
			}
			
		}else{
			p.sendMessage(plugin.prefix+noSelected);
		}
	}
	
	public void createNewArena(Player p, String arena){
		
		if(!getArena(arena).exists()){
			
			plugin.getSelectedArena().setSelectedArena(p.getName(), arena);
			
			p.sendMessage(plugin.prefix + "Arena creation started! (Arena: "+arena+")");
			p.sendMessage(plugin.prefix + "To check the arenas status type /witherarena checkdata");
			p.sendMessage(plugin.prefix + "To setup the arena type /witherarena set");
		}else{
			
			p.sendMessage(plugin.prefix + ChatColor.RED + "Error: That arena already exist!");
			
		}
		

		
	}
	
	
	
	public void listArenas(Player p){
		
		if(plugin.getConfig().contains("arenas")){
			p.sendMessage(plugin.prefix + ChatColor.GREEN + "Arenas: " + ChatColor.GRAY +getArenas().toString().replace("]", "").replace("[", ""));
			
		}else{
			p.sendMessage(plugin.prefix + ChatColor.GREEN + "Arenas: " + ChatColor.GRAY +"No arenas to list");
		}
		
		
		
	}
	
	public void readyPlayer(Player p, String arena){
		plugin.getPlayerManager().getPlayer(p.getName()).setReady(true);
		p.sendMessage(plugin.prefix + "You have been flagged as ready!");
		
		if(checkIfAllReady(arena)){
			startArena(arena);
			
		}
		
	}
	
	
	
	public boolean checkAlreadyCreated(String arena){
		if(plugin.getConfig().contains("arenas")){
			Set<String> arenas = plugin.getConfig().getConfigurationSection("arenas").getKeys(false);
		
			if(arenas.contains(arena)){
				return true;
			}else{
				return false;
			}
			
			
		}else{
			return false;
		}
	}
	
	private WorldGuardPlugin getWorldGuard() {
	    Plugin pl = plugin.getServer().getPluginManager().getPlugin("WorldGuard");
	 
	    // WorldGuard may not be loaded
	    if (pl == null || !(pl instanceof WorldGuardPlugin)) {
	        return null; // Maybe you want throw an exception instead
	    }
	 
	    return (WorldGuardPlugin) pl;
	}
	
	private boolean checkEmptyInventory(Player p){
		
		for(ItemStack item : p.getInventory().getContents())
		{
		    if(item != null)
		      return false;
		}
		return true;
		
	}
	
	private boolean checkEmptyArmor(Player p){
		
		for(ItemStack item : p.getInventory().getArmorContents())
		{
		    if(item.getTypeId() != 0)
		      return false;
		}
		return true;
		
	}
	
	private void setupRegion(String region, World w){
		
		ProtectedRegion reg = getWorldGuard().getRegionManager(w).getRegion(region);
		reg.setFlag(DefaultFlag.PVP, State.DENY);
	}
	
	
	
}
