package me.icyrelic.com.Manager.Arena;

import java.util.ArrayList;
import java.util.Set;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import me.icyrelic.com.WitherArena;

public class ZombieManager {
	
	WitherArena plugin;
	public ZombieManager(WitherArena instance) {

		plugin = instance;

	}
	String noSelected = (ChatColor.RED + "Error: No arena selected!");
	
	public final ZombieSpawn getZombieSpawn(String spawn, String arena){
		return new ZombieSpawn(spawn, arena, plugin);
	}
	
	public ArrayList<String> getZombieSpawns(String arena){
		return getZombieSpawn("",arena).list();
	}
	
    public void loadZombieSpawns(){
    	
		if(plugin.getConfig().contains("arenas")){
			Set<String> arenas = plugin.getConfig().getConfigurationSection("arenas").getKeys(false);
			String arenastostr = arenas.toString().replace("[", "").replace("]", "");
			String[] str = arenastostr.split(", ");
			int x = 0;
			while (x < str.length){
				
				if(plugin.getConfig().contains("arenas."+str[x]+".spawns")){
					Set<String> spawns = plugin.getConfig().getConfigurationSection("arenas."+str[x]+".spawns.zombie").getKeys(false);
					String spawnstostr = spawns.toString().replace("[", "").replace("]", "");
					String[] spawnstr = spawnstostr.split(", ");
					int y = 0;
					while (y < spawnstr.length){
						
						String loc = plugin.getConfig().getString("arenas."+str[x]+".spawns.zombie."+spawnstr[y]+".location");
						
						String[] xyz = loc.split(", ");
						
						float pitch = plugin.getConfig().getInt("arenas."+str[x]+".spawns.zombie."+spawnstr[y]+".pitch");
						float yaw = plugin.getConfig().getInt("arenas."+str[x]+".spawns.zombie."+spawnstr[y]+".yaw");
						String world = plugin.getConfig().getString("arenas."+str[x]+".spawns.zombie."+spawnstr[y]+".world");
						
						plugin.getArenaManager().getArena(str[x]).setHasZombieSpawns(true);
						
						getZombieSpawn(str[x], spawnstr[y]).add(Integer.parseInt(xyz[0]), Integer.parseInt(xyz[1]), Integer.parseInt(xyz[2]), pitch, yaw, world);
						
						y++;
					}
					
				}
				
				
				x++;
			}
			
		}
		
		

    	
    }
    
	public void addZombieSpawn(String spawn, Location loc, Player p){
		
		if(plugin.getSelectedArena().hasSelectedArena(p.getName())){
			String arena = plugin.getSelectedArena().getSelectedArena(p.getName());
			
			if(plugin.getArenaManager().checkAlreadyCreated(arena)){
				editZombieSpawn(p, spawn);
			}else{
				
				plugin.getSelectedArena().addSpawn(spawn, arena, loc, p.getWorld().getName(), loc.getPitch(), loc.getYaw());
				p.sendMessage(plugin.prefix + "Zombie Spawn Added For Arena: " + arena);

				plugin.getArenaManager().checkCreate(arena, p);
			}
			
			

			
		}else{
			p.sendMessage(plugin.prefix+noSelected);
		}
		
	}
    
	
	
	public void editZombieSpawn(Player p, String spawn){
		if(plugin.getSelectedArena().hasSelectedArena(p.getName())){
			String arena = plugin.getSelectedArena().getSelectedArena(p.getName());
			
			String zombiestr = (p.getLocation().getBlockX()+", "+p.getLocation().getBlockY()+", "+p.getLocation().getBlockZ());
			
			plugin.getArenaManager().getArena(arena).setHasZombieSpawns(true);
			getZombieSpawn(spawn, arena).add(p.getLocation().getBlockX(), p.getLocation().getBlockY(), p.getLocation().getBlockZ(), p.getLocation().getPitch(), p.getLocation().getYaw(), p.getLocation().getWorld().getName());
			
			if(plugin.getConfig().contains("arenas."+arena+".spawns.zombie."+spawn)){
				p.sendMessage(plugin.prefix + "Zombie Spawn Updated For Arena: " + arena);
			}else{
				p.sendMessage(plugin.prefix + "Zombie Spawn Added For Arena: " + arena);
			}
			
			plugin.getConfig().set("arenas."+arena+".spawns.zombie."+spawn+".location", zombiestr);
			plugin.getConfig().set("arenas."+arena+".spawns.zombie."+spawn+".pitch", p.getLocation().getPitch());
			plugin.getConfig().set("arenas."+arena+".spawns.zombie."+spawn+".yaw", p.getLocation().getYaw());
			plugin.getConfig().set("arenas."+arena+".spawns.zombie."+spawn+".world", p.getWorld().getName());
			plugin.saveConfig();

			
		}else{
			p.sendMessage(plugin.prefix+noSelected);
		}
	}
    

}
