package me.icyrelic.com.Manager.Arena;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import me.icyrelic.com.WitherArena;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.plugin.Plugin;

import com.sk89q.worldguard.bukkit.WorldGuardPlugin;

public class SelectedArena {

	WitherArena plugin;
	public SelectedArena(WitherArena instance) {

		plugin = instance;

		}
	
	private static HashMap<String, String> selectedArena = new HashMap<String, String>();
	
	private static HashMap<String, Location> spawns = new HashMap<String, Location>();
	private static HashMap<String, String> spawnsArena = new HashMap<String, String>();
	private static HashMap<String, String> spawnsworld = new HashMap<String, String>();
	private static HashMap<String, Float> spawnspitch = new HashMap<String, Float>();
	private static HashMap<String, Float> spawnsyaw = new HashMap<String, Float>();
	
	private static HashMap<String, Location> lobbyWarp = new HashMap<String, Location>();
	private static HashMap<String, String> lobbyworld = new HashMap<String, String>();
	private static HashMap<String, Float> lobbypitch = new HashMap<String, Float>();
	private static HashMap<String, Float> lobbyyaw = new HashMap<String, Float>();
	
	
	private static HashMap<String, Location> arenaWarp = new HashMap<String, Location>();
	private static HashMap<String, String> arenaworld = new HashMap<String, String>();
	private static HashMap<String, Float> arenapitch = new HashMap<String, Float>();
	private static HashMap<String, Float> arenayaw = new HashMap<String, Float>();
	
	private static HashMap<String, Location> specWarp = new HashMap<String, Location>();
	private static HashMap<String, String> specworld = new HashMap<String, String>();
	private static HashMap<String, Float> specpitch = new HashMap<String, Float>();
	private static HashMap<String, Float> specyaw = new HashMap<String, Float>();
	
	private static HashMap<String, Location> witherSpawn = new HashMap<String, Location>();
	private static HashMap<String, String> witherworld = new HashMap<String, String>();
	private static HashMap<String, Float> witherpitch = new HashMap<String, Float>();
	private static HashMap<String, Float> witheryaw = new HashMap<String, Float>();
	
	
	private static HashMap<String, String> wgRegion = new HashMap<String, String>();
	
	
	public String getSelectedArena(String player){
		
		return selectedArena.get(player);
	}
	
	public boolean setSelectedArena(String player, String arena){
		if(selectedArena.containsKey(player)){
			selectedArena.remove(player);
		}
		
		selectedArena.put(player, arena);
		
		return true;
		
	}
	
	public boolean hasSelectedArena(String player){
		if(selectedArena.containsKey(player)){
			return true;
		}else{
			return false;
		}
		
		
	}
	
	public String checkWarps(String arena){
		
		String str = "";
		
		if(!lobbyWarp.containsKey(arena)){
			str = str+"Lobby ";
		}
		
		if(!arenaWarp.containsKey(arena)){
			str = str+"Arena ";
		}
		
		if(!specWarp.containsKey(arena)){
			str = str+"Spectator ";
		}

		
		
		return str;
	}
	
	public String checkSpawn(String arena){
		
		String str = "";
		
		if(!witherSpawn.containsKey(arena)){
			str = str+"Wither ";
		}
		if(!spawns.containsValue(arena)){
			str = str+"ZombieSpawn(s) ";
		}
		
		return str;
	}
	
	public String checkRegion(String arena){
		
		String str = "";
		
		if(!wgRegion.containsKey(arena)){
			str = str+"Region ";
		}
		
		return str;
	}
	
	public void clear(String arena){
		lobbyWarp.remove(arena);
		lobbyworld.remove(arena);
		lobbypitch.remove(arena);
		lobbyyaw.remove(arena);
		
		int x = 0;
		
		while(x < zombieSpawns(arena).size()){
			spawns.remove(zombieSpawns(arena).get(x));
			x++;
		}
		
		
		arenaWarp.remove(arena);
		arenaworld.remove(arena);
		arenapitch.remove(arena);
		arenayaw.remove(arena);
		
		specWarp.remove(arena);
		specworld.remove(arena);
		specpitch.remove(arena);
		specyaw.remove(arena);
		
		witherSpawn.remove(arena);
		witherworld.remove(arena);
		witherpitch.remove(arena);
		witheryaw.remove(arena);
		
		wgRegion.remove(arena);
	}
	
	public void addSpawn(String spawn, String arena, Location loc, String world, Float pitch, Float yaw){
		if(spawns.containsKey(arena+"_"+spawn)){
			spawns.remove(arena+"_"+spawn);
			spawnsArena.remove(arena+"_"+spawn);
			spawnsworld.remove(arena+"_"+spawn);
			spawnspitch.remove(arena+"_"+spawn);
			spawnsyaw.remove(arena+"_"+spawn);
		}

		spawns.put(arena+"_"+spawn, loc);
		spawnsArena.put(arena+"_"+spawn, arena);
		spawnsworld.put(arena+"_"+spawn, world);
		spawnspitch.put(arena+"_"+spawn, pitch);
		spawnsyaw.put(arena+"_"+spawn, yaw);

		
	}
	
	public ArrayList<Object> getSpawn(String spawnName, String arena){
		
		ArrayList<Object> spawn = new ArrayList<Object>();
		
		spawn.clear();
		spawn.add(spawns.get(arena+"_"+spawnName));
		spawn.add(spawnsworld.get(arena+"_"+spawnName));
		spawn.add(spawnspitch.get(arena+"_"+spawnName));
		spawn.add(spawnsyaw.get(arena+"_"+spawnName));
		
		
		return spawn;
	}
	
	public void setLobby(String arena, Location loc, String world, Float pitch, Float yaw){
		
		if(lobbyWarp.containsKey(arena)){
			lobbyWarp.remove(arena);
		}
		lobbyworld.put(arena, world);
		lobbyWarp.put(arena, loc);
		lobbypitch.put(arena, pitch);
		lobbyyaw.put(arena, yaw);

		
	}
	
	public boolean canCreate(String arena){
		
		if(lobbyWarp.containsKey(arena) && arenaWarp.containsKey(arena) && specWarp.containsKey(arena)
				 && witherSpawn.containsKey(arena) && wgRegion.containsKey(arena) && spawnsArena.containsValue(arena)){
			
			return true;
			
		}else{
			return false;
		}
	}
	
	public ArrayList<Object> getLobby(String arena){
		
		ArrayList<Object> lobby = new ArrayList<Object>();
		
		lobby.clear();
		lobby.add(lobbyWarp.get(arena));
		lobby.add(lobbyworld.get(arena));
		lobby.add(lobbypitch.get(arena));
		lobby.add(lobbyyaw.get(arena));
		
		return lobby;
		
	}
	
	public ArrayList<Object> getArena(String arena){
		
		ArrayList<Object> arena1 = new ArrayList<Object>();
		
		arena1.clear();
		arena1.add(arenaWarp.get(arena));
		arena1.add(arenaworld.get(arena));
		arena1.add(arenapitch.get(arena));
		arena1.add(arenayaw.get(arena));
		
		return arena1;
		
	}
	
	public ArrayList<Object> getSpec(String arena){
		
		ArrayList<Object> spec = new ArrayList<Object>();
		
		spec.clear();
		spec.add(specWarp.get(arena));
		spec.add(specworld.get(arena));
		spec.add(specpitch.get(arena));
		spec.add(specyaw.get(arena));
		
		return spec;
		
	}
	
	public ArrayList<Object> getWitherSpawn(String arena){
		
		ArrayList<Object> witherspawn = new ArrayList<Object>();
		
		witherspawn.clear();
		witherspawn.add(witherSpawn.get(arena));
		witherspawn.add(witherworld.get(arena));
		witherspawn.add(witherpitch.get(arena));
		witherspawn.add(witheryaw.get(arena));
		
		return witherspawn;
		
	}
	
	public String getRegion(String arena){
		return wgRegion.get(arena);
	}
	
	
	public void setArena(String arena, Location loc, String world, Float pitch, Float yaw){
		if(arenaWarp.containsKey(arena)){
			arenaWarp.remove(arena);
		}
		arenaworld.put(arena, world);
		arenaWarp.put(arena, loc);
		arenapitch.put(arena, pitch);
		arenayaw.put(arena, yaw);
	}
	
	public void setSpec(String arena, Location loc, String world, Float pitch, Float yaw){
		if(specWarp.containsKey(arena)){
			specWarp.remove(arena);
		}
		specworld.put(arena, world);
		specWarp.put(arena, loc);
		specpitch.put(arena, pitch);
		specyaw.put(arena, yaw);
	}
	
	public void witherSpawn(String arena, Location loc, String world, Float pitch, Float yaw){
		if(witherSpawn.containsKey(arena)){
			witherSpawn.remove(arena);
		}
		witherworld.put(arena, world);
		witherSpawn.put(arena, loc);
		witherpitch.put(arena, pitch);
		witheryaw.put(arena, yaw);
	}
	
	public boolean setRegion(String arena, String region, World world){
		if(wgRegion.containsKey(arena)){
			wgRegion.remove(arena);
		}
		
		
		boolean isRegion = getWorldGuard().getRegionManager(world).hasRegion(region);
		
		if(isRegion){
			wgRegion.put(arena, region);
			return true;
		}else{
			return false;
		}
	}
	
	
	private WorldGuardPlugin getWorldGuard() {
	    Plugin pl = plugin.getServer().getPluginManager().getPlugin("WorldGuard");
	 
	    // WorldGuard may not be loaded
	    if (pl == null || !(pl instanceof WorldGuardPlugin)) {
	        return null; // Maybe you want throw an exception instead
	    }
	 
	    return (WorldGuardPlugin) pl;
	}
	
	
	public ArrayList<String> zombieSpawns(String arena){
		ArrayList<String> spawnsforarena = new ArrayList<String>();
		
		for (Entry<String, String> entry : spawnsArena.entrySet()) { 
			
			if(entry.getValue().equals(arena)){
				spawnsforarena.add(entry.getKey());
			}
			
		}
		
		return spawnsforarena;
		
	}
	
	
}
