package me.icyrelic.com.Manager.Arena;

import me.icyrelic.com.WitherArena;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class Zombies extends BukkitRunnable{

		private static String arena = "";

		
		WitherArena plugin;
		
	    public Zombies(String arenaName, WitherArena instance) {
	       arena = arenaName;
	       plugin = instance;
	    }
	 
	    public void run() {
	        // What you want to schedule goes here
	    	if(plugin.getArenaManager().getArena(arena).isRunning()){
	    		
	    		if(plugin.getArenaManager().getArena(arena).hasZombieSpawns()){
	    			int size = 0;
	    			
		            int x = 0;
		            while(x < plugin.getPlayerManager().getPlayer("").list().size()){
		            	Player p = plugin.getServer().getPlayer(plugin.getPlayerManager().getPlayer("").list().get(x));
		            	p.sendMessage(plugin.prefix + "Zombies are spawning");
		            	x++;
		            }
	    			
		    		while(size < plugin.getZombieManager().getZombieSpawns(arena).size()){
		    			
		    			String spawn = plugin.getZombieManager().getZombieSpawns(arena).get(size);
		    			World w = plugin.getServer().getWorld(plugin.getZombieManager().getZombieSpawn(arena, spawn).getWorld());
		    			int x1 = plugin.getZombieManager().getZombieSpawn(arena, spawn).getBlockX();
		    			int y = plugin.getZombieManager().getZombieSpawn(arena, spawn).getBlockY();
		    			int z = plugin.getZombieManager().getZombieSpawn(arena, spawn).getBlockZ();
		    			float pitch = plugin.getZombieManager().getZombieSpawn(arena, spawn).getPitch();
		    			float yaw = plugin.getZombieManager().getZombieSpawn(arena, spawn).getYaw();
		    			
		    			Location loc = new Location(w, x1, y, z);
			    		
			    		loc.setPitch(pitch);
			            loc.setYaw(yaw);
			            loc.add(0.5, 0, 0.5);
			            int zomperspwn = plugin.getMaster().getZombiesPerSpawn();
			            
			            int spawned = 0;
			            
			            while(spawned < zomperspwn){
			            	Entity entity = w.spawnEntity(loc, EntityType.ZOMBIE);
				            int entityID = entity.getEntityId();
				            
			    			plugin.getArenaManager().getArena(arena).addZombie(entityID);
			    			spawned++;
			            }
			            

		    			size++;
		    		}
	    		}
	    		
	    		
	    		
	    	}else{
	    		this.cancel();
	    	}
	       
	        
	    }
	
	    
}
