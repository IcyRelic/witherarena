package me.icyrelic.com.Manager.Arena;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import me.icyrelic.com.WitherArena;

public class ZombieSpawn {

	WitherArena plugin;
	private static String spawn = "";
	private static String arena = "";
	private static HashMap<String, String> spawns = new HashMap<String, String>();
	private static HashMap<String, Integer> spawnsx = new HashMap<String, Integer>();
	private static HashMap<String, Integer> spawnsy = new HashMap<String, Integer>();
	private static HashMap<String, Integer> spawnsz = new HashMap<String, Integer>();
	private static HashMap<String, Float> spawnspitch = new HashMap<String, Float>();
	private static HashMap<String, Float> spawnsyaw = new HashMap<String, Float>();
	private static HashMap<String, String> spawnsworld = new HashMap<String, String>();
	
	public ZombieSpawn(String spawnName, String arenaName, WitherArena instance) {
		spawn = spawnName;
		arena = arenaName;
		plugin = instance;
	}
	public ArrayList<String> list(){
		ArrayList<String> spawnsforarena = new ArrayList<String>();
		
		System.out.println(plugin.getArenaManager().getArena(arena).hasZombieSpawns());
		
		spawnsforarena.clear();
		Set<String> spawns = plugin.getConfig().getConfigurationSection("arenas."+arena+".spawns.zombie").getKeys(false);
		String spawnstostr = spawns.toString().replace("[", "").replace("]", "");
		String[] spawnstr = spawnstostr.split(", ");
		int y = 0;
		while (y < spawnstr.length){
			
			spawnsforarena.add(spawnstr[y]);
			y++;
			
		}
		
		return spawnsforarena;
		
	}
	public boolean add(int x, int y, int z, float pitch, float yaw, String world){
		if(spawns.containsKey(arena+"_"+spawn)){
			spawns.remove(arena+"_"+spawn);
			spawnsx.remove(arena+"_"+spawn);
			spawnsy.remove(arena+"_"+spawn);
			spawnsx.remove(arena+"_"+spawn);
			spawnspitch.remove(arena+"_"+spawn);
			spawnsyaw.remove(arena+"_"+spawn);
			spawnsworld.remove(arena+"_"+spawn);
		}
		if(!exists()){
			spawns.put(arena+"_"+spawn, arena);
			spawnsx.put(arena+"_"+spawn, x);
			spawnsy.put(arena+"_"+spawn, y);
			spawnsz.put(arena+"_"+spawn, z);
			spawnspitch.put(arena+"_"+spawn, pitch);
			spawnsyaw.put(arena+"_"+spawn, yaw);
			spawnsworld.put(arena+"_"+spawn, world);
			return true;
		}else{
			return false;
		}
	}
	
	
	public int getBlockX(){
		if(exists()){
			return spawnsx.get(arena+"_"+spawn);
		}else{
			return 0;
		}
	}
	
	public int getBlockY(){
		if(exists()){
			return spawnsy.get(arena+"_"+spawn);
		}else{
			return 0;
		}
	}
	
	public int getBlockZ(){
		if(exists()){
			return spawnsz.get(arena+"_"+spawn);
		}else{
			return 0;
		}
	}
	
	public String getWorld(){
		if(exists()){
			return spawnsworld.get(arena+"_"+spawn);
		}else{
			return "Unknown";
		}
	}
	
	public float getYaw(){
		if(exists()){
			return spawnsyaw.get(arena+"_"+spawn);
		}else{
			return 0;
		}
	}
	
	public float getPitch(){
		if(exists()){
			return spawnsyaw.get(arena+"_"+spawn);
		}else{
			return 0;
		}
	}
	
	public boolean exists(){
		if(spawns.containsKey(arena+"_"+spawn)){
			return true;
		}else{
			return false;
		}
		
	}
	
}
