package me.icyrelic.com.Manager.Player;

import java.util.ArrayList;

import me.icyrelic.com.WitherArena;

public class PlayerManager {

	WitherArena plugin;
	public PlayerManager(WitherArena instance) {

		plugin = instance;

	}
	
	public final ArenaPlayer getPlayer(String playerName){
		return new ArenaPlayer(playerName);
	}
	
	public ArrayList<String> getPlayers(){
		return getPlayer("").list();
	}
	
	
	
}
