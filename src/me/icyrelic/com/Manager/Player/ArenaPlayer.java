package me.icyrelic.com.Manager.Player;

import java.util.ArrayList;
import java.util.HashMap;

public class ArenaPlayer {
	
	private static String playerName = "";
	
	private static ArrayList<String> players = new ArrayList<String>();
	private static HashMap<String, String> playerArenas = new HashMap<String, String>();
	private static HashMap<String, Boolean> readyPlayers = new HashMap<String, Boolean>();
	private static HashMap<String, Boolean> lobbyPlayers = new HashMap<String, Boolean>();
	
	public ArenaPlayer(String name) {
		playerName = name;
	}

	public ArrayList<String> list(){
		return players;
	}
	
	public String getPlayerArena(){
		return playerArenas.get(playerName);
	}
	
	public Boolean isReady(){
		return readyPlayers.get(playerName);
	}
	public Boolean setReady(Boolean ready){
		
		if(readyPlayers.containsKey(playerName)){
			readyPlayers.remove(playerName);
			readyPlayers.put(playerName, ready);
			return true;
		}else{
			return false;
		}
		
	}
	
	public Boolean isInArena(){
		if(playerArenas.containsKey(playerName)){
			return true;
		}else{
			return false;
		}
	}
	
	public Boolean isInLobby(){
		return lobbyPlayers.get(playerName);
	}
	
	public boolean removeFromLobby(){
		lobbyPlayers.remove(playerName);
		return true;
	}
	
	public Boolean addPlayer(String arena){
		if(!isInArena()){
			players.remove(playerName);
			playerArenas.remove(playerName);
			readyPlayers.remove(playerName);
			lobbyPlayers.remove(playerName);
			players.add(playerName);
			playerArenas.put(playerName, arena);
			readyPlayers.put(playerName, false);
			lobbyPlayers.put(playerName, true);
			return true;
		}else{
			return false;
		}
	}
	
	public Boolean removeFromArena(){
		if(isInArena()){
			players.remove(playerName);
			playerArenas.remove(playerName);
			readyPlayers.remove(playerName);
			lobbyPlayers.remove(playerName);
			return true;
		}else{
			return false;
		}
	}
}
