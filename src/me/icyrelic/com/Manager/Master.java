package me.icyrelic.com.Manager;

import me.icyrelic.com.WitherArena;

public class Master {
	
	WitherArena plugin;
	public Master(WitherArena instance) {

		plugin = instance;

		}
	
	public int getZombiesPerSpawn(){
		return plugin.getConfig().getInt("global.zombies_per_spawn");
	}
	
	public boolean isAllowed(String cmd){
		
		if(plugin.getConfig().getString("global.allowed_commands").contains(cmd)){
			return true;
		}else{
			return false;
		}
		
	}

}
