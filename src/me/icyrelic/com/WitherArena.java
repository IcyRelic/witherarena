package me.icyrelic.com;

import me.icyrelic.com.Manager.Master;
import me.icyrelic.com.Manager.Arena.ArenaManager;
import me.icyrelic.com.Manager.Arena.SelectedArena;
import me.icyrelic.com.Manager.Arena.ZombieManager;
import me.icyrelic.com.Manager.Class.ClassManager;
import me.icyrelic.com.Manager.Player.PlayerManager;
import me.icyrelic.com.Commands.WitherArenaCommand;
import me.icyrelic.com.Listners.ArenaListeners;
import me.icyrelic.com.Listners.SignEvents;

import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class WitherArena extends JavaPlugin {

	/**
	 * To Do
	 * rewards
	 * done.
	 */
	
	
	public String prefix = (ChatColor.GRAY+"["+ChatColor.DARK_RED+"WitherArena"+ChatColor.GRAY+"] "+ChatColor.WHITE);
	
	
	public void onEnable(){
		loadConfiguration();
		getArenaManager().loadArenas();
		getClassManager().loadClasses();
		getZombieManager().loadZombieSpawns();
		
		getServer().getPluginManager().registerEvents(new SignEvents(this), this);
		getServer().getPluginManager().registerEvents(new ArenaListeners(this), this);
		getCommand("witherarena").setExecutor(new WitherArenaCommand(this));
		
		
		
	}
	
	public void onDisable(){
		
		while(!getArenaManager().getArena("").list().isEmpty()){
			String arena = getArenaManager().getArena("").list().get(0);
			if(getArenaManager().getArena(arena).isRunning()){
				String world = getConfig().getString("arenas."+arena+".spawns.wither.world");
				World w = getServer().getWorld(world);
				getArenaManager().removeWither(arena, w);
				getArenaManager().removeZombies(arena, w);
				getArenaManager().getArena(arena).setRunning(false);
			}
			getArenaManager().getArena(arena).deleteArena();
		}

		while(!getPlayerManager().getPlayer("").list().isEmpty()){
			Player p = getServer().getPlayer(getPlayerManager().getPlayer("").list().get(0));
			p.getInventory().clear();
			p.getInventory().setArmorContents(null);
			getArenaManager().spectateArena(p, getPlayerManager().getPlayer(p.getName()).getPlayerArena());
			
			getPlayerManager().getPlayer(p.getName()).removeFromArena();
		}
		

		
	}
	
	
	public ArenaManager getArenaManager(){
		return new ArenaManager(this);
	}
	public ClassManager getClassManager(){
		return new ClassManager(this);
	}
	public PlayerManager getPlayerManager(){
		return new PlayerManager(this);
	}
	public SelectedArena getSelectedArena(){
		return new SelectedArena(this);
	}
	public ZombieManager getZombieManager(){
		return new ZombieManager(this);
	}
	public Master getMaster(){
		return new Master(this);
	}
	
	
	public void loadConfiguration(){
	    getConfig().options().copyDefaults(true);
	    saveConfig();
	
	}
}
